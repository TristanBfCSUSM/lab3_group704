﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Lab3.models;

namespace Lab3
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<TasksModels> tasks { get; set; }
        public MainPage()
        {
            InitializeComponent();
            tasks = new ObservableCollection<TasksModels>();
            MyList.ItemsSource = tasks;

            MyList.RefreshCommand = new Command(() => {

                //Do stuff here
                MyList.IsRefreshing = false;
            });

        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            var rand = new Random();
            var ListTasks = new List<TasksModels>();
            var title = "item" + rand.Next(0, 100).ToString();

            ListTasks.Add(new TasksModels { productName = title, productDescription = "ntm" });
            MyList.ItemsSource = ListTasks;
        }
    }
}